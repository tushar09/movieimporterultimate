/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movieimporterultimate;

import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author WTush
 */
public class Utils {

    public static String getMacAddress() {
        InetAddress ip;
        try {

            ip = InetAddress.getLocalHost();
            //System.out.println("Current IP address : " + ip.getHostAddress());

            NetworkInterface network = NetworkInterface.getByInetAddress(ip);

            byte[] mac = network.getHardwareAddress();

            //System.out.print("Current MAC address : ");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }

            return sb.toString();

        } catch (UnknownHostException e) {

            e.printStackTrace();

        } catch (SocketException e) {

            e.printStackTrace();

        }
        return "null";
    }

    public static String getLinkFromFrameCode(String fc) {

        if (fc.startsWith("<iframe")) {
            String extracted[] = fc.split("\"");
            //fc = fc.replaceAll("<iframe src=\"", "");
            //fc = fc.replaceAll("\" width=\"320\" height=\"132\" frameborder=\"0\" scrolling=\"no\" allowfullscreen></iframe>", "");
            fc = extracted[1];
            fc = fc.replace("embed?", "download?");
            return fc;
        } else {
            return "Invalid Link";
        }

    }

    public static String getFileSize(String url) {
        HttpURLConnection connection;
        String finalUrl = url;
        try {
            do {
                connection = (HttpURLConnection) new URL(finalUrl)
                        .openConnection();
                connection.setInstanceFollowRedirects(false);
                connection.setUseCaches(false);
                connection.setRequestMethod("GET");
                connection.connect();
                int responseCode = connection.getResponseCode();
                if (responseCode >= 300 && responseCode < 400) {
                    String redirectedUrl = connection.getHeaderField("Location");
                    if (null == redirectedUrl) {
                        break;
                    }
                    finalUrl = redirectedUrl;
                    //System.out.println("redirected url: " + finalUrl);
                } else {
                    break;
                }
            } while (connection.getResponseCode() != HttpURLConnection.HTTP_OK);
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return humanReadableByteCount(Long.parseLong(getSize(finalUrl)), true);
    }

    public static String[] getOmdbiapi(String name, String year) {

        String url = "http://www.omdbapi.com/?t=";

        name = name.replaceAll(" ", "+");
        name = name.replaceAll(",", "%2c");

        url += name + "&y=" + year + "&plot=full&r=json";
        System.out.println(url);
        
        return loadData(url);
    }
    
    
    
    
    
    
    
    
    
    

    //for Use of Own Class Methods
    private static String[] loadData(String url) {

        String s[] = new String[15];
        try {

            JSONObject jo = readJsonFromUrl(url);
            s[0] = jo.getString("Title");
            s[1] = jo.getString("Year");
            s[2] = jo.getString("Genre");
            s[3] = jo.getString("Plot");
            s[4] = jo.getString("imdbRating");
            s[5] = jo.getString("Poster");

        } catch (JSONException e1) {
            System.out.println(e1.toString());

        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return s;
    }

    private static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }
    
    private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

    private static String getSize(String link) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(link);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("HEAD");
            conn.getInputStream();
            return "" + conn.getContentLength();
        } catch (IOException e) {
            return "-1";
        } finally {
            conn.disconnect();
        }
    }

    private static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) {
            return bytes + " B";
        }
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f%sB", bytes / Math.pow(unit, exp), pre);
    }

}
